"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Todo = void 0;
class Todo {
    constructor(id, todo, checked) {
        this.id = id;
        this.todo = todo;
        this.checked = checked;
    }
}
exports.Todo = Todo;
//# sourceMappingURL=todo.model.js.map