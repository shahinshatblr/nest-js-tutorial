"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TodoController = void 0;
const common_1 = require("@nestjs/common");
const todos_service_1 = require("./todos.service");
let TodoController = class TodoController {
    constructor(todoservice) {
        this.todoservice = todoservice;
    }
    addTodo(todo, checked) {
        let todoRes = this.todoservice.insertTodo(todo, checked);
        return { "todos": todoRes, "message": "todo added successfully" };
    }
    getTodos() {
        return { "todos": this.todoservice.getTodos() };
    }
    getTodo(todoId) {
        return this.todoservice.getTodo(todoId);
    }
    updateTodo(todoId, todo, checked) {
        console.log(todoId, todo, checked, "incoming");
        let updatedTodo = this.todoservice.updateTodo(todoId, todo, checked);
        return { "todos": updatedTodo, "message": "todo updated successfully" };
    }
    deletTodo(todoId) {
        return this.todoservice.deleteTodo(todoId);
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Body('todo')), __param(1, common_1.Body('checked')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Boolean]),
    __metadata("design:returntype", Object)
], TodoController.prototype, "addTodo", null);
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], TodoController.prototype, "getTodos", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], TodoController.prototype, "getTodo", null);
__decorate([
    common_1.Put(':id'),
    __param(0, common_1.Param('id')), __param(1, common_1.Body('todo')), __param(2, common_1.Body('checked')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Boolean]),
    __metadata("design:returntype", Object)
], TodoController.prototype, "updateTodo", null);
__decorate([
    common_1.Delete(":id"),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], TodoController.prototype, "deletTodo", null);
TodoController = __decorate([
    common_1.Controller("todo"),
    __metadata("design:paramtypes", [todos_service_1.TodoService])
], TodoController);
exports.TodoController = TodoController;
//# sourceMappingURL=todos.controller.js.map