import { TodoService } from "./todos.service";
export declare class TodoController {
    private readonly todoservice;
    constructor(todoservice: TodoService);
    addTodo(todo: string, checked: Boolean): any;
    getTodos(): {
        todos: import("./todo.model").Todo[];
    };
    getTodo(todoId: string): {
        id: string;
        todo: string;
        checked: Boolean;
    };
    updateTodo(todoId: string, todo: string, checked: Boolean): any;
    deletTodo(todoId: string): {
        message: string;
        todos: import("./todo.model").Todo[];
    };
}
