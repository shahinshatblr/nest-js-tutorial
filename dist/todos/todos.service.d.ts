import { Todo } from "./todo.model";
export declare class TodoService {
    private todo;
    insertTodo(todo: string, checked: Boolean): Todo[];
    getTodos(): Todo[];
    getTodo(todoId: string): {
        id: string;
        todo: string;
        checked: Boolean;
    };
    updateTodo(todoId: string, todo: string, checked: Boolean): Todo[];
    deleteTodo(todoId: string): {
        message: string;
        todos: Todo[];
    };
}
