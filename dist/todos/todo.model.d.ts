export declare class Todo {
    id: string;
    todo: string;
    checked: Boolean;
    constructor(id: string, todo: string, checked: Boolean);
}
