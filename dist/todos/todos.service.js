"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TodoService = void 0;
const common_1 = require("@nestjs/common");
const todo_model_1 = require("./todo.model");
let TodoService = class TodoService {
    constructor() {
        this.todo = [];
    }
    insertTodo(todo, checked) {
        let todoId = Math.floor(100000 + Math.random() * 900000).toString();
        let newTodo = new todo_model_1.Todo(todoId, todo, checked);
        this.todo.push(newTodo);
        return this.todo;
    }
    getTodos() {
        return [...this.todo];
    }
    getTodo(todoId) {
        let todo = this.todo.find((item) => todoId === item.id);
        if (!todo) {
            throw new common_1.NotFoundException("could not find todo");
        }
        return Object.assign({}, todo);
    }
    updateTodo(todoId, todo, checked) {
        let index = this.todo.findIndex((item) => todoId === item.id);
        console.log(index, "indexx");
        if (index !== -1) {
            let todoTemp = Object.assign({}, this.todo[index]);
            console.log(todoTemp, "item");
            todoTemp.todo = todo;
            todoTemp.checked = checked;
            console.log(todoTemp, "item2");
            this.todo[index] = todoTemp;
            return this.todo;
        }
        else {
            throw new common_1.NotFoundException("could not find");
        }
    }
    deleteTodo(todoId) {
        let index = this.todo.findIndex((item) => todoId === item.id);
        if (index !== -1) {
            this.todo.splice(index, 1);
            return { "message": "Deleted successfully", "todos": this.todo };
        }
        else {
            throw new common_1.NotFoundException("could not find");
        }
    }
};
TodoService = __decorate([
    common_1.Injectable()
], TodoService);
exports.TodoService = TodoService;
//# sourceMappingURL=todos.service.js.map