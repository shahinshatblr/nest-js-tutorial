import { Controller, Post, Body ,Get,Param,Put,Delete} from '@nestjs/common';
import {TodoService} from "./todos.service"

@Controller("todo")
export class TodoController {
constructor(private readonly todoservice:TodoService){}
  @Post()
  addTodo(@Body('todo') todo:string , @Body('checked') checked:Boolean): any  {
   let  todoRes = this.todoservice.insertTodo(todo,checked);
     return {"todos":todoRes,"message":"todo added successfully"}
  }
  @Get()
  getTodos(){
      return {"todos":this.todoservice.getTodos()}
  }
  @Get(':id')
  getTodo(@Param('id') todoId :string){
   return this.todoservice.getTodo(todoId)
  }
@Put(':id')
updateTodo(@Param('id') todoId :string ,@Body('todo') todo:string , @Body('checked') checked:Boolean ):any{
    console.log(todoId,todo,checked,"incoming")
    let updatedTodo =  this.todoservice.updateTodo(todoId,todo,checked)
    return {"todos":updatedTodo,"message":"todo updated successfully"}
}
@Delete(":id")
deletTodo(@Param('id') todoId :string){
    return  this.todoservice.deleteTodo(todoId)
}

}