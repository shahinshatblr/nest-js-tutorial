import { Injectable, NotFoundException } from '@nestjs/common';
import { Todo } from "./todo.model"
@Injectable()
export class TodoService {
    private todo: Todo[] = []

    insertTodo(todo: string, checked: Boolean) {
        let todoId = Math.floor(100000 + Math.random() * 900000).toString();
        let newTodo = new Todo(todoId, todo, checked)
        this.todo.push(newTodo)
        return this.todo
    }

    getTodos() {
        return [...this.todo]
    }
    getTodo(todoId: string) {
        let todo = this.todo.find((item) => todoId === item.id)
        if (!todo) {
            throw new NotFoundException("could not find todo")
        }
        return { ...todo }
    }
    updateTodo(todoId: string,todo: string, checked: Boolean){
      let  index =  this.todo.findIndex((item) => todoId === item.id)
      console.log(index,"indexx")
      if (index !== -1){
        let todoTemp = {...this.todo[index]}
        console.log(todoTemp,"item")
        todoTemp.todo =  todo
        todoTemp.checked =  checked
        console.log(todoTemp,"item2")
        
        this.todo[index] =  todoTemp
        return this.todo

      }else
      {
        throw new NotFoundException("could not find")
      }
    }
    deleteTodo(todoId: string){
        let  index =  this.todo.findIndex((item) => todoId === item.id)
        if (index !== -1){
           this.todo.splice(index,1)
           return {"message":"Deleted successfully","todos":this.todo}
          }else
          {
            throw new NotFoundException("could not find")
          }
    }
}